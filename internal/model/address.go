package model

type Address struct {
	Id       int32  `db:"id"`
	UserId   int32  `db:"user_id"`
	City     string `db:"city"`
	Province string `db:"province"`
	Country  string `db:"country"`
}
