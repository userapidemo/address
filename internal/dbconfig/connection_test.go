package dbconfig

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	_ "github.com/newrelic/go-agent/_integrations/nrpq"
	"github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestConnection(t *testing.T) {
	convey.Convey("Given information to connect into database ", t, func() {
		const connStr = "user=%s password=%s dbname=%s host=%s port=%d sslmode=disable"
		DBURL := fmt.Sprintf(connStr,
			"postgres",
			"123456",
			"userapi",
			"localhost",
			5432)
		convey.Convey("When passing infomation to sqlx.Open() function", func() {
			_, err := sqlx.Open("nrpostgres", DBURL)
			convey.Convey("Then database connection error should be nil", func() {
				convey.So(err, convey.ShouldEqual, nil)
			})
		})
	})
}
