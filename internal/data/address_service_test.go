package data

import (
	"bitbucket.org/userapidemo/address/internal/dbconfig"
	"bitbucket.org/userapidemo/address/internal/model"
	"context"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestCreateAddress(t *testing.T) {
	Convey("Given address information to save into DB ", t, func() {
		addrInfo := model.Address{
			UserId:   1,
			City:     "dhaka",
			Province: "dhaka",
			Country:  "BD",
		}
		service := NewDataService(dbconfig.GetConnSQLX())
		Convey("When calling CreateAddress method", func() {
			id, err := service.CreateAddress(context.Background(), &addrInfo)
			Convey("Then the method should not return error", func() {
				So(err, ShouldBeNil)
			})
			Convey("Then the method should return a id which is not zero", func() {
				So(id, ShouldNotEqual, 0)
			})
		})
	})
}
