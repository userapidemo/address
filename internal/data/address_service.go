package data

import (
	"bitbucket.org/userapidemo/address/internal/model"
	"context"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type AddressRepo interface {
	CreateAddress(ctx context.Context, request *model.Address) (int32, error)
}

type address struct {
	conn *sqlx.DB
}

func NewDataService(db *sqlx.DB) AddressRepo {
	return &address{
		conn: db,
	}
}

func (u *address) CreateAddress(ctx context.Context, request *model.Address) (int32, error) {
	tx := u.conn.MustBegin()
	var id int32
	err := tx.QueryRowx("INSERT INTO address_info.address (user_id,city,province,country) VALUES ($1,$2,$3,$4) returning id",
		request.UserId, request.City, request.Province, request.Country).Scan(&id)

	if err != nil {
		tx.Rollback()
		return 0, errors.Wrap(err, "insert address error")
	}
	tx.Commit()
	return id, nil
}
