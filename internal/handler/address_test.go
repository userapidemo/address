package handler

import (
	"bitbucket.org/userapidemo/address/internal/data"
	"bitbucket.org/userapidemo/address/internal/dbconfig"
	proto "bitbucket.org/userapidemo/address/pkg/generated/api"
	"context"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

var userRequest = &proto.AddressRequest{}
var UserResponse = &proto.AddressResponse{}

var handler1 = &handler{svc: data.NewDataService(dbconfig.GetConnSQLX())}

func TestCreateAddress(t *testing.T) {
	Convey("Given address information to save into DB", t, func() {

		req := &proto.AddressRequest{UserId: 1, City: "Dhaka", Province: "Dhaka", Country: "BD"}
		res := &proto.AddressResponse{}
		Convey("When calling CreatedAddress method", func() {
			err := handler1.CreateAddress(context.Background(), req, res)
			Convey("Then the method should not return error and error should be nil", func() {
				So(err, ShouldEqual, nil)
			})
			Convey("Then the method should return success message", func() {
				So(res.Status, ShouldEqual, "success")
			})
			Convey("Then the method should return a id and it should not be zero", func() {
				So(res.AddressId, ShouldNotEqual, 0)
			})
		})
	})
}
