package handler

import (
	"bitbucket.org/userapidemo/address/internal/data"
	"bitbucket.org/userapidemo/address/internal/model"
	proto "bitbucket.org/userapidemo/address/pkg/generated/api"
	"context"
	"github.com/asim/go-micro/v3/util/log"
	"github.com/jmoiron/sqlx"
)

type handler struct {
	svc data.AddressRepo
}

func NewAddressService(conn *sqlx.DB) proto.AddressServiceHandler {
	return &handler{
		svc: data.NewDataService(conn),
	}
}

func (h *handler) CreateAddress(ctx context.Context, req *proto.AddressRequest, res *proto.AddressResponse) error {

	address := &model.Address{UserId: req.UserId, City: req.City, Province: req.Province, Country: req.Country}

	id, err := h.svc.CreateAddress(ctx, address)
	if err != nil {
		log.Fatal("Didn't able to create the user: ")
		res.Status = "failed"
		return nil
	}
	res.AddressId = id
	res.Status = "success"

	return nil
}
