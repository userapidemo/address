package main

import (
	"bitbucket.org/userapidemo/address/internal/dbconfig"
	"bitbucket.org/userapidemo/address/internal/handler"
	"bitbucket.org/userapidemo/address/pkg/generated/api"
	"github.com/asim/go-micro/v3"
	"github.com/pkg/errors"
	"log"
	"os"
)

func main() {
	service := micro.NewService(
		micro.Name("sf.address.service"),
		micro.Address(":8001"),
	)
	service.Init()

	dbconn := dbconfig.GetConnSQLX()
	h := handler.NewAddressService(dbconn)

	api.RegisterAddressServiceHandler(service.Server(), h)

	if err := service.Run(); err != nil {
		log.Fatalln(nil, errors.Wrap(err, "unable to run service"))
		os.Exit(0)
	}
	os.Exit(0)
}
