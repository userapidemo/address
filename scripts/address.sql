CREATE SCHEMA address_info
CREATE TABLE address_info.address (
                                      id         serial ,
                                      user_id    int NOT NULL ,
                                      city 	   varchar(20),
                                      province   varchar(20),
                                      country    varchar(20),
                                      PRIMARY KEY ( id )
)