package main

import (
	proto "bitbucket.org/userapidemo/address/pkg/generated/api"
	"context"
	"fmt"
	"github.com/asim/go-micro/v3"
	"github.com/asim/go-micro/v3/util/log"
)

func main() {
	client := micro.NewService()
	service := proto.NewAddressService("sf.address.service", client.Client())
	res, err := service.CreateAddress(context.Background(), &proto.AddressRequest{
		UserId:   1,
		City:     "Khilkhet",
		Province: "Dhaka-1200",
		Country:  "Bangladesh",
	})
	if err != nil {
		log.Fatal("Got error ==> CreateAddress : ", err)
	}
	fmt.Println(res.AddressId)
	fmt.Println(res.Status)
}
